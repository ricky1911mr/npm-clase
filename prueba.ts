//para importar interfaces
import{usuariosInterface}from './usuario-interfaz'
import{correoInterfaz}from './correo-interfaz'
import{direccionInterfaz}from './direccion-interfaz'  
import{conTodo}from './conTodo-interfaz'

import{sumar}from './sumar'
import{Persona}from './clase-persona'
import{Estudiante}from './clase-estudiante'
import{usuario}from './usuario-base'
import{UsuarioEpn}from './usuario-epn'
import{UsuarioPolicia}from './usuario-policia'
import{Usuariosupermaxi}from './usuario-supermaxi'

/* const edad:number=55;
const nombre:string = 'Test'
let numero:number;
const estado:boolean= true;
const arrreglo:Array<number | string | boolean> = [2, '', '234'];
const arreglo2:(number|string)[]=[2,'234'];
let variableCualquiera: any */

/* function sumar(a?:number,b?:number):number{
    let num1= 4;
    let num2= 5;
    return num1 +num2
    if(a){
        num1=10
    }
    if (b){
        num2 =20
    }
    return num1 +num2
}*/

sumar(2,4);
sumar(2);
sumar(); 

const persona:object={
    nombre: 'Ricardo',
    apellido: 'Paredes'
}

/* class  Persona{
    private _nombre:string; ///VCariables privadas con _ al inicio
    private _apellido:string;
    public edad:number;
    public edad:number;
    constructor(protected nombre?:string, public apellido?:string){
        this.nombre=nombre;
        this.apellido=apellido;
        this.edad=this.edad;
    }
    get nombre(){
        return this._nombre;
    }
    set nombre(nombre:string){
        this._nombre=nombre;
    } 
    get apellido(){
        return this._apellido
    }
    set apellido(apellido:string){
        this._apellido=apellido;
    }

}
*/
const nuevaPersona: Persona = new Persona('otro','nombre');

console.log('persona',persona);
console.log('persona',nuevaPersona);  
/* class Estudiante extends Persona{
    constructor(public nombre:string, public apellido:string, public carrera:string){
        super(nombre, apellido)
        carrera=carrera;
    }
} */
const estudiante =new Estudiante('df','we','asdasda')
console.log('estudiante',estudiante)

/* class usuario{
    constructor(protected nombreU:string, protected apellidoU:string, protected cedulaU:string){
        this.nombreU=nombreU;
        this.apellidoU=apellidoU;
        this.cedulaU=cedulaU;
    }
} */
/* class UsuarioEpn extends usuario{
    constructor(public nombre:string, public apellido:string, public cedula:string, public carrera:string){
        super(nombre,apellido,cedula)
        carrera=carrera;
    }
} */
const usuarioepn =new UsuarioEpn('Pepito','Perez','1708394570', 'Ing Sistemas')
console.log('Usuario Poli',usuarioepn.cedula)

/* class UsuarioPolicia extends usuario{
    constructor(public nombre:string, public apellido:string, public cedula:string, public cargo:string){
        super(nombre,apellido,cedula)
        cargo=cargo;
    }
} */
const usuariopolicia =new UsuarioPolicia('Pablo','Altamirano','1725905440', 'Teniente')
console.log('Usuario Policia',usuariopolicia.nombre)

/* class Usuariosupermaxi extends usuario{
    constructor(public nombre:string, public apellido:string, public cedula:string, public tipo:string){
        super(nombre,apellido,cedula)
        tipo=tipo;
    }
} */

const usuariosuper =new Usuariosupermaxi('Raul','Reyes','1706568755', 'Cliente')
console.log('Usuario supermaxi',usuariosuper.apellido)

/* interface usuariosInterface{  ///Usar interfaces ahorra espacio de memoria, tambien ayuda a tener ordenado
    nombre?: string;            /// las variables y tambien es mas facil analizar y refactorar errores
    apellido?:string;
    cambiarMayusculas():string
} */

const usuariosInterfaz: usuariosInterface= {
    nombre: 'Ricardo',
    apellido: 'Paredes',
    cambiarMayusculas:()=>{return 'fd'}
}
console.log('Usuario interfaz antes',usuariosInterfaz)
usuariosInterfaz.nombre= 'Andres'
usuariosInterfaz.apellido= 'Villacis'
console.log('Usuario interfaz despues',usuariosInterfaz)

/* interface correoInterfaz{
    correo:string;
    enviarCorreo():boolean;
} */

/* interface direccionInterfaz{
direccion:string;
} */

/* interface conTodo extends correoInterfaz, direccionInterfaz,usuariosInterface{
    estaCompleto: boolean;
} */


class usuarioCualquiera implements conTodo{
    estaCompleto: boolean;    correo: string;
    enviarCorreo(): boolean {
        throw new Error("Method not implemented.");
    }
    direccion: string;
    nombre?: string;
    apellido?: string;
    cambiarMayusculas(): string {
        throw new Error("Method not implemented.");
    }
}