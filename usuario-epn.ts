import{usuario}from './usuario-base'
export class UsuarioEpn extends usuario{
    constructor(public nombre:string, public apellido:string, public cedula:string, public carrera:string){
        super(nombre,apellido,cedula)
        carrera=carrera;
    }
}