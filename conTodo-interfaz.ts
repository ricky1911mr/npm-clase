import{usuariosInterface}from './usuario-interfaz'
import{correoInterfaz}from './correo-interfaz'
import{direccionInterfaz}from './direccion-interfaz'

export interface conTodo extends correoInterfaz, direccionInterfaz,usuariosInterface{
    estaCompleto: boolean;
}