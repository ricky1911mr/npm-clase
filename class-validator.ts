import {validate, Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";

class usuario{
    public edad:string
    constructor(public nombre:string, public apellido:string, protected cedula:string){
        this.nombre=nombre;
        this.apellido=apellido;
        this.cedula=cedula;
        this.edad=this.edad
    }
}

const usuarioValidar = new usuario('Ricardo','Paredes','1725905440');

validate(usuarioValidar).then(respuestaVAlidacion => { // errors is an array of validation errors
    if (respuestaVAlidacion.length > 0) {
        console.log('Hay error')
    } else {
        console.log('Validacion correcta, el arreglo esta vacio')
        //Validacion correcta
        //me llega un arreglo vacio 
    }
});

//Validar una clase que yo creo 
// la clase debe tener varios atributos para poder validar cada uno


// https://github.com/typestack/class-validator

//Revisar tambien esa pag porque nos va a averiguar el funcionamiento