import{usuario}from './usuario-base'
export class UsuarioPolicia extends usuario{
    constructor(public nombre:string, public apellido:string, public cedula:string, public cargo:string){
        super(nombre,apellido,cedula)
        cargo=cargo;
    }
}