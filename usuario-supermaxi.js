"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var usuario_base_1 = require("./usuario-base");
var Usuariosupermaxi = /** @class */ (function (_super) {
    __extends(Usuariosupermaxi, _super);
    function Usuariosupermaxi(nombre, apellido, cedula, tipo) {
        var _this = _super.call(this, nombre, apellido, cedula) || this;
        _this.nombre = nombre;
        _this.apellido = apellido;
        _this.cedula = cedula;
        _this.tipo = tipo;
        tipo = tipo;
        return _this;
    }
    return Usuariosupermaxi;
}(usuario_base_1.usuario));
exports.Usuariosupermaxi = Usuariosupermaxi;
