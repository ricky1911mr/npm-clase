"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var usuario_base_1 = require("./usuario-base");
var UsuarioEpn = /** @class */ (function (_super) {
    __extends(UsuarioEpn, _super);
    function UsuarioEpn(nombre, apellido, cedula, carrera) {
        var _this = _super.call(this, nombre, apellido, cedula) || this;
        _this.nombre = nombre;
        _this.apellido = apellido;
        _this.cedula = cedula;
        _this.carrera = carrera;
        carrera = carrera;
        return _this;
    }
    return UsuarioEpn;
}(usuario_base_1.usuario));
exports.UsuarioEpn = UsuarioEpn;
