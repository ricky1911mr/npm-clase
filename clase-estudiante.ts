import{Persona}from './clase-persona'
export class Estudiante extends Persona{
    constructor(public nombre:string, public apellido:string, public carrera:string){
        super(nombre, apellido)
        carrera=carrera;
    }
}